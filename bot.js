const Discord = require('discord.js');
const client = new Discord.Client();
const config = require("./config.json")
const SQLite = require("better-sqlite3");
const sql = new SQLite('./money.sqlite');
const items = new SQLite('./items.sqlite')
const prefix = config.prefix
let cooldown = {}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

client.on("ready", () => {
    console.log('Starting EcoBot..')
    client.user.setActivity('Starting Bot..', { type: 'PLAYING' });
    // Check if the table "points" exists.
    const table = sql.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'money';").get();
    if (!table['count(*)']) {
      // If the table isn't there, create it and setup the database correctly.
      sql.prepare("CREATE TABLE money (id TEXT PRIMARY KEY, user TEXT, money INTEGER);").run();
      // Ensure that the "id" row is always unique and indexed.
      sql.prepare("CREATE UNIQUE INDEX idx_money_id ON money (id);").run();
      sql.pragma("synchronous = 1");
      sql.pragma("journal_mode = wal");
    }

    const itemTable = items.prepare("SELECT count(*) FROM sqlite_master WHERE type='table' AND name = 'items';").get();
    if (!itemTable['count(*)']) {
        items.prepare("CREATE TABLE items (guildid TEXT, itemname TEXT, emote TEXT, price INTEGER, code TEXT);").run();
        // items.prepare("CREATE UNIQUE INDEX idx_items_id ON items (guildid);").run();
        items.pragma("synchronous = 1");
        items.pragma("journal_mode = wal");
    }
  
    // And then we have two prepared statements to get and set the score data.
    client.getScore = sql.prepare("SELECT * FROM money WHERE user = ?");
    client.setScore = sql.prepare("INSERT OR REPLACE INTO money (id, user, money) VALUES (@id, @user, @money);");
    client.makeItem = items.prepare("INSERT INTO items (guildid, itemname, emote, price, code) VALUES (@guildid, @itemname, @emote, @price, @code);");
    client.getItem = items.prepare("SELECT * FROM items WHERE guildid = ? AND itemname = ?")
    client.getAllItems = items.prepare("SELECT * FROM items AS allItems").get()
    console.log('Started! Logged in as '+client.user.tag)
    client.user.setActivity('over '+client.guilds.size+' servers! | e!help', { type: 'WATCHING' });
    console.log('Currently in '+client.guilds.size+' servers!')
  });

client.on('guildCreate', guild => {
    client.user.setActivity('over '+client.guilds.size+' servers! | e!help', { type: 'WATCHING' });
});

client.on('message', msg => {
    if (msg.author.bot) return;
    let money;
    if (msg.guild) {
      money = client.getScore.get(msg.author.id);
      if (!money) {
        money = { id: `${msg.author.id}`, user: msg.author.id, money: 100}
      }
      money.money += 0.5;
      client.setScore.run(money);
    }
    if (msg.content.indexOf(config.prefix) !== 0) return;

    const args = msg.content.slice(config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();





    if(msg.content.startsWith(prefix+'ping')){
        msg.channel.send('** :ping_pong: | Pong! Current ping is '+client.ping+'ms.**')
    }

    if(msg.content.startsWith(prefix+'help')){
        const embed = {
            "title": "Commands",
            "color": 4122923,
            "timestamp": "2018-07-03T01:14:59.757Z",
            "footer": {
              "icon_url": "https://cdn.discordapp.com/attachments/463144151647518721/463517144202608640/euro.png",
              "text": "Thanks for using EcoBot, the global economy bot!"
            },
            "thumbnail": {
              "url": "https://cdn.discordapp.com/attachments/463144151647518721/463517144202608640/euro.png"
            },
            "author": {
              "name": "EcoBot",
              "icon_url": "https://cdn.discordapp.com/attachments/463144151647518721/463517144202608640/euro.png"
            },
            "fields": [
              {
                "name": "About / Help",
                "value": "e!help - Shows this embed.\ne!about - Tells you about the Discord Bot."
              },
              {
                "name": "Economy Commands",
                "value": "e!money or e!bal - Shows you the amount of money you have.\ne!work - Work for your money! Usable every 5 minutes.\ne!bank - GLOBAL amount of money (all users' money combined)\ne!total @user - Tells you how much the selected user has.\ne!pay [amount] [user] - Pays the selected user the selected amount to the user.\ne!createitem - Creates an item. PERMS: MANAGE_GUILD. Usage: e!createitem [:emote:] [name] [price] [code (discord.js)]\ne!deleteitem - Deletes an item. PERMS: MANAGE_GUILD Usage: e!deleteitem [name]"
              },
              {
                "name": "Misc. Commands",
                "value": "e!ping - Ping from the bot server to the Discord API."
              }
            ]
          };
          msg.author.send({ embed });
    }


    if (msg.content.startsWith(prefix+'money') || msg.content.startsWith(prefix+'bal')) {
        msg.channel.send('** :moneybag: | You currently have e$'+money.money+'!**')
    }
    
    if(msg.content.startsWith(prefix+'give')) {
        // Limited to guild owner - adjust to your own preference!
        if(!msg.author.id === "401792058970603539") return msg.channel.send('** :x: | You are not the bot developer!**')
        const user = msg.mentions.users.first() || client.users.get(args[0]);
        if(!user) return msg.reply("You must mention someone or give their ID!");
        const pointsToAdd = parseInt(args[1], 10);
        if(!pointsToAdd) return msg.channel.send("** :x: | You didn't tell me how much money to give**")
        // Get their current points.
        let userscore = client.getScore.get(user.id);
        // It's possible to give points to a user we haven't seen, so we need to initiate defaults here too!
        if (!userscore) {
          userscore = { id: `${user.id}`, user: user.id, money: 100}
        }
        userscore.money += pointsToAdd;
        // We also want to update their level (but we won't notify them if it changes)
        let userLevel = Math.floor(0.1 * Math.sqrt(money.money));
        userscore.level = userLevel;
        // And we save it!
        client.setScore.run(userscore);
        return msg.channel.send(`** :money_with_wings: | ${user.tag} has received e$${pointsToAdd}!**`);
      }

    if(msg.content.startsWith(prefix+'work')) {
        if (!cooldown[msg.author.id]) {
            cooldown[msg.author.id] = true
            let userscore = client.getScore.get(msg.author.id);
            let amount = getRandomInt(20,120)
            userscore.money += amount
            client.setScore.run(userscore)
            msg.channel.send('** :money_mouth: | PAYDAY! You have recieved e$'+amount+'!**')
            setTimeout(() => {
                cooldown[msg.author.id] = false
            }, 300000) // five min
        } else {
            msg.channel.send('** :x: | You have to wait the 5 minutes!**')
        }
    }

    if(msg.content.startsWith(prefix+'about')) {
        msg.author.send('**EcoBot is a global currency bot for Discord, made in Discord.js and SQLite. Some slight Discord.JS knowledge is needed to make items. Created by AmusedGrape#0001.**')
    }

    if(msg.content.startsWith(prefix+'bank')){
        var qry = sql.prepare("SELECT SUM(money) AS total FROM money").get()
        total = qry.total
        msg.channel.send('** :earth_americas: | The current *GLOBAL* bank is worth e$'+total+'.**')
    }
    
    if(msg.content.startsWith(prefix+'total')){
        const user = msg.mentions.users.first() || client.users.get(args[0]);
        if (user.bot) return;
        var userTotal = client.getScore.get(user.id)
        msg.channel.send('** :moneybag: | '+user.tag+' has a total of e$'+userTotal+'.**')
    }

    if(msg.content.startsWith(prefix+'pay')) {
        const user = msg.mentions.users.first() || client.users.get(args[0]);
        if(!user) return msg.reply("You must mention someone or give their ID!");
        const pointsToTake = parseInt(args[1], 10);
        if(!pointsToTake) return msg.channel.send("** :x: | You didn't tell me how much money to give**")
        let userscore = client.getScore.get(user.id);
        if (!userscore) {
          userscore = { id: `${user.id}`, user: user.id, money: 100}
        }
        userscore.money -= pointsToTake;
        client.setScore.run(userscore);
        return msg.channel.send(`** :money_with_wings: | ${msg.author.username} has paid e$${pointsToTake} to ${user.tag}!**`);
    }

    if (msg.content.startsWith(prefix+'createitem')) {
        if (msg.member.hasPermission("MANAGE_GUILD")) {
            const emote = args[0]
            const name = args[1]
            const price = args[2]
            const code = args[3]
            // let list = client.getAllItems.allItems
            item = { guildid: `${msg.guild.id}`, itemname: `${name}`, emote: `${emote}`, price: `${price}`, code: `${code}`}
            //(guildid, itemname, emote, price, code)
            client.makeItem.run(item)
            item = 0
            msg.channel.send('** :white_check_mark: | Created item!**')
        } else {
            msg.channel.send('** :x: | No Perms! Permissions to use: `MANAGE_GUILD`.**')
        }
    }

    if(msg.content.startsWith(prefix+'buy')){
        const itemToBuy = args[0]
        let list = items.prepare("SELECT * FROM items WHERE itemname = ?").get(itemToBuy)
        var item = client.getItem.get(msg.guild.id,itemToBuy)
        let userscore = client.getScore.get(msg.author.id);
        if (!userscore) {
            userscore = { id: `${msg.author.id}`, user: msg.author.id, money: 100}
        }
        
        if (item) {
            userscore.money -= list.price
            client.setScore.run(userscore)
            msg.channel.send(msg.author.tag+', you have bought `'+itemToBuy+'`!')
            eval(list.code)
        }
    }
    
    if(msg.content.startsWith(prefix+'deleteitem')){
        if (msg.member.hasPermission("MANAGE_GUILD")) {
            const itemToDelete = args[0]
            let toDelete = items.prepare("SELECT * FROM items WHERE itemname = ?").get(itemToDelete)
            items.prepare("DELETE FROM items WHERE itemname = "+itemToDelete)
            msg.channel.send('** :white_check_mark: | Deleted item!**')
        }
    }

    if(msg.content.startsWith(prefix+'items')){
        let itemlist = items.prepare("SELECT * FROM items").all(msg.guild.id)
        msg.channel.send(itemlist)
    }
});

client.login('NDYzMTQzNzQ4NDY2MTE0NTcw.DhsH-A.XXhFiH5HcNIqbX4JjR4QTwJfEW4');

// https://discordapp.com/oauth2/authorize?client_id=463143748466114570&scope=bot&permissions=2080795752